import type { NextPage } from "next";
import Image from "next/image";
import Layout from "../components/Layout";

const Home: NextPage = () => {
  return (
    <Layout pageTitle="Homepage">
      <Image
        src="/ales-krivec-y0i1lh-T0_w-unsplash.jpg"
        width={200}
        height={180}
        alt="nature"
      />
      <h1 className="title">Welcome Jamaludin</h1>
    </Layout>
  );
};

export default Home;
